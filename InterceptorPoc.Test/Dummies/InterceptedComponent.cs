﻿using System;
using InterceptorPoc.Logging;

namespace InterceptorPoc.Test.Dummies
{
    public class InterceptedComponent : IInterceptedComponent
    {
        private readonly int _returnValue;

        public InterceptedComponent(int returnValue)
        {
            _returnValue = returnValue;
        }

        public int Calculate(string input, string input2)
        {
            Logger.Current.Log("Real method called");
            return _returnValue;
        }
    }
}