﻿// ReSharper disable InconsistentNaming

using System;

namespace InterceptorPoc.Test.Dummies
{
    public class ComplexComponent
    {
        private readonly ICrossRateProvider _crossRateProvider;

        public ComplexComponent(ICrossRateProvider crossRateProvider)
        {
            _crossRateProvider = crossRateProvider;
        }

        public virtual CrossRate FetchCrossRate(Currency @from, Currency to)
        {
            return _crossRateProvider.Fetch(@from, @to);
        }
    }

    public class CrossRate
    {
        public Currency From { get; set; }

        public Currency To { get; set; }

        public decimal Rate { get; set; }

        public DateTime CreatedOn { get; set; }
    }

    public enum Currency
    {
        Undefined,
        USD,
        AUD,
        CAD,
        EUR,
        CNH,
        CNF
    }
}