﻿using InterceptorPoc.Logging;

namespace InterceptorPoc.Test.Dummies
{
    public class VoidComponent : IVoidComponent
    {
        public void DoIt()
        {
            Logger.Current.Log("DoIt called on real implementation");
        }
    }
}