﻿namespace InterceptorPoc.Test.Dummies
{
    public interface IVoidComponent
    {
        void DoIt();
    }
}
