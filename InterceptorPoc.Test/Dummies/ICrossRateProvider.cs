﻿namespace InterceptorPoc.Test.Dummies
{
    public interface ICrossRateProvider
    {
        CrossRate Fetch(Currency from, Currency to);
    }
}