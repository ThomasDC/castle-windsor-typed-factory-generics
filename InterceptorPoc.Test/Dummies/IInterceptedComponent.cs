﻿namespace InterceptorPoc.Test.Dummies
{
    public interface IInterceptedComponent
    {
        int Calculate(string input, string input2);
    }
}