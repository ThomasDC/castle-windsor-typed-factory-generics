﻿using System;
using CacheManager.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using InterceptorPoc.Logging;
using InterceptorPoc.Test.Dummies;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Rhino.Mocks;

namespace InterceptorPoc.Test
{
    [TestClass]
    public class CacheInterceptorTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Logger.Current = new TraceLogger();
        }

        [TestMethod]
        public void CacheInterceptor_DoesntReturnCachedResult_WhenConfiguredNotToCacheMethods()
        {
            // Arrange
            const int realReturnValue = 42;
            const int cachedReturnValue = 69;

            // Act
            var returnValue = CallCalculate(CachingBehavior.DontCache, realReturnValue, cachedReturnValue);

            // Assert
            Assert.AreEqual(realReturnValue, returnValue);
        }

        [TestMethod]
        public void CacheInterceptor_ReturnsCachesResult_WhenConfiguredToCacheAllMethodsAndResultInCache()
        {
            // Arrange
            const int realReturnValue = 42;
            const int cachedReturnValue = 69;

            // Act
            var returnValue = CallCalculate(CachingBehavior.CacheAllMethods, realReturnValue, cachedReturnValue);

            // Assert
            Assert.AreEqual(cachedReturnValue, returnValue);
        }

        [TestMethod]
        public void CacheInterceptor_DoesntInterceptCallsToVoidMethods()
        {
            // Arrange
            var mockedCacheManager = MockRepository.GenerateMock<ICacheManager<string>>();

            var container = new WindsorContainer();
            container.Register(
                Component.For<CacheInterceptor>().DependsOn(Dependency.OnValue<CachingBehavior>(CachingBehavior.CacheAllMethods)),
                Component.For<ICacheManager<string>>().Instance(mockedCacheManager),
                Component.For<IVoidComponent>().ImplementedBy<VoidComponent>().Interceptors<CacheInterceptor>());

            mockedCacheManager.Expect(_ => _.Get(string.Empty, string.Empty)).IgnoreArguments().Repeat.Never();

            var sut = container.Resolve<IVoidComponent>();

            // Act
            sut.DoIt();

            // Assert
            mockedCacheManager.VerifyAllExpectations();
        }

        private static int CallCalculate(CachingBehavior behavior, int realReturnValue, int cachedReturnValue)
        {
            // Arrange
            var mockedCacheManager = MockRepository.GenerateMock<ICacheManager<string>>();

            var container = new WindsorContainer();
            container.Register(
                Component.For<CacheInterceptor>().DependsOn(Dependency.OnValue<CachingBehavior>(behavior)),
                Component.For<ICacheManager<string>>().Instance(mockedCacheManager),
                Component.For<IInterceptedComponent>().ImplementedBy<InterceptedComponent>().DependsOn(Dependency.OnValue<int>(realReturnValue)).Interceptors<CacheInterceptor>());
            const string inputArgument1 = "foobar", inputArgument2 = "baroof";
            mockedCacheManager.Expect(_ => _.Get(JsonConvert.SerializeObject(new object[] { inputArgument1, inputArgument2 }), "Calculate"))
                .Return(JsonConvert.SerializeObject(cachedReturnValue));

            var sut = container.Resolve<IInterceptedComponent>();

            // Act
            return sut.Calculate(inputArgument1, inputArgument2);
        }

        [TestMethod]
        public void CacheInterceptor_CanCacheComplexObjects()
        {
            // Arrange
            var mockedCrossRateProvider = MockRepository.GenerateMock<ICrossRateProvider>();
            var firstCrossRate = new CrossRate { From = Currency.EUR, To = Currency.USD, Rate = 0.85m, CreatedOn = DateTime.Now.AddMinutes(-1337) };
            mockedCrossRateProvider.Expect(_ => _.Fetch(Currency.EUR, Currency.USD))
                .Return(firstCrossRate)
                .Repeat.Once();
            var secondCrossRate = new CrossRate { From = Currency.EUR, To = Currency.USD, Rate = 0.91m, CreatedOn = DateTime.Now };
            mockedCrossRateProvider.Expect(_ => _.Fetch(Currency.EUR, Currency.USD))
                .Return(secondCrossRate)
                .Repeat.Once();

            var cacheManager = CacheFactory.Build<string>(p => p.WithDictionaryHandle().WithExpiration(ExpirationMode.None, TimeSpan.MaxValue));

            var container = new WindsorContainer();
            container.Register(
                Component.For<ICrossRateProvider>().Instance(mockedCrossRateProvider),
                Component.For<ComplexComponent>().Interceptors<CacheInterceptor>(),
                Component.For<CacheInterceptor>().DependsOn(Dependency.OnValue<CachingBehavior>(CachingBehavior.CacheAllMethods)),
                Component.For<ICacheManager<string>>().Instance(cacheManager));

            var sut = container.Resolve<ComplexComponent>();

            // Act
            var firstReturnValue = sut.FetchCrossRate(Currency.EUR, Currency.USD);
            var secondReturnValue = sut.FetchCrossRate(Currency.EUR, Currency.USD);
            cacheManager.Clear();
            var thirdReturnValue = sut.FetchCrossRate(Currency.EUR, Currency.USD);

            // Assert
            Assert.IsNotNull(firstReturnValue);
            Assert.AreEqual(firstCrossRate.From, firstReturnValue.From);
            Assert.AreEqual(firstCrossRate.To, firstReturnValue.To);
            Assert.AreEqual(firstCrossRate.Rate, firstReturnValue.Rate);
            Assert.AreEqual(firstCrossRate.CreatedOn, firstReturnValue.CreatedOn);

            Assert.IsNotNull(secondReturnValue);
            Assert.AreEqual(firstCrossRate.From, secondReturnValue.From);
            Assert.AreEqual(firstCrossRate.To, secondReturnValue.To);
            Assert.AreEqual(firstCrossRate.Rate, secondReturnValue.Rate);
            Assert.AreEqual(firstCrossRate.CreatedOn, secondReturnValue.CreatedOn);

            Assert.IsNotNull(thirdReturnValue);
            Assert.AreEqual(secondCrossRate.From, thirdReturnValue.From);
            Assert.AreEqual(secondCrossRate.To, thirdReturnValue.To);
            Assert.AreEqual(secondCrossRate.Rate, thirdReturnValue.Rate);
            Assert.AreEqual(secondCrossRate.CreatedOn, thirdReturnValue.CreatedOn);
        }
    }
}
