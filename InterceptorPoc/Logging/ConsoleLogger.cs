﻿using System;
using System.Threading;

namespace InterceptorPoc.Logging
{
    public class ConsoleLogger : Logger
    {
        protected override void LogInternal(string message, string memberName, string className)
        {
            Console.WriteLine($"[{DateTime.Now}] [{Thread.CurrentThread.ManagedThreadId}] [{className}] [{memberName}] {message}");
        }
    }
}