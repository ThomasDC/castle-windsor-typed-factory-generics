﻿using System.IO;

namespace InterceptorPoc.Logging
{
    public abstract class Logger
    {
        static Logger()
        {
            ResetToDefault();
        }

        public static Logger Current { get; set; }

        public void Log(string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "")
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(sourceFilePath);
            LogInternal(message, memberName, fileNameWithoutExtension);
        }

        protected abstract void LogInternal(string message, string memberName, string className);

        public static void ResetToDefault()
        {
            Current = new TraceLogger();
        }
    }
}