﻿using System;
using System.Diagnostics;
using System.Threading;

namespace InterceptorPoc.Logging
{
    public class TraceLogger : Logger
    {
        protected override void LogInternal(string message, string memberName, string className)
        {
            Trace.WriteLine($"[{DateTime.Now}] [{Thread.CurrentThread.ManagedThreadId}] [{className}] [{memberName}] {message}");
        }
    }
}