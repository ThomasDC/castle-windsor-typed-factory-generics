﻿using System;

namespace InterceptorPoc
{
    /// <summary>
    /// Enables result caching of a method by the <see cref="CacheInterceptor"/> 
    /// in combination with the <see cref="CachingBehavior.CacheAnnotatedMethods"/> setting.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class CacheResultAttribute : Attribute
    {

    }
}
