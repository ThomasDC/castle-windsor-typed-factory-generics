﻿namespace InterceptorPoc
{
    /// <summary>
    /// Defines the caching behavior to apply when intercepting a method call.
    /// </summary>
    public enum CachingBehavior
    {
        /// <summary>
        /// Don't cache any intercepted method calls.
        /// </summary>
        DontCache,
        /// <summary>
        /// Only cache intercepted method calls annotated with the <see cref="CacheResultAttribute"/>.
        /// </summary>
        CacheAnnotatedMethods,
        /// <summary>
        /// Cache all intercepted method calls, regardless of the <see cref="CacheResultAttribute"/>.
        /// </summary>
        CacheAllMethods
    }
}