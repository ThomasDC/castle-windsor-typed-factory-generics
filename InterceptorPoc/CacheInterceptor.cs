﻿using System;
using System.Linq;
using CacheManager.Core;
using CacheManager.Core.Internal;
using Castle.DynamicProxy;
using InterceptorPoc.Logging;
using Newtonsoft.Json;

namespace InterceptorPoc
{
    /// <summary>
    /// Responsible for caching AOP.
    /// Intercepts method calls to an encapsulated component and applies caching behavior where applicable.
    /// </summary>
    public class CacheInterceptor : IInterceptor
    {
        private readonly ICacheManager<string> _cacheManager;
        private readonly CachingBehavior _cachingBehavior;

        public CacheInterceptor(ICacheManager<string> cacheManager, CachingBehavior cachingBehavior)
        {
            _cacheManager = cacheManager;
            _cachingBehavior = cachingBehavior;

            _cacheManager.OnAdd += CacheManagerOnAdd;
            _cacheManager.OnGet += CacheManagerOnGet;
            _cacheManager.OnPut += CacheManagerOnPut;
            _cacheManager.OnRemove += CacheManagerOnRemove;
            _cacheManager.OnUpdate += CacheManagerOnUpdate;
        }
        
        public void Intercept(IInvocation invocation)
        {
            var cacheEnabled = IsCacheEnabledForThisCall(invocation);

            if (cacheEnabled)
            {
                var key = JsonConvert.SerializeObject(invocation.Arguments);
                var region = invocation.Method.Name;
                var cachedReturnValue = _cacheManager.Get(key, region);
                if (cachedReturnValue == null)
                {
                    Logger.Current.Log($"No cached result found for key '{key}' and region '{region}'");
                    invocation.Proceed();
                    var realResult = invocation.ReturnValue;
                    var value = JsonConvert.SerializeObject(realResult);
                    _cacheManager.Add(key, value, region);
                }
                else
                {
                    Logger.Current.Log($"Cached result found for key '{key}' and region '{region}': '{cachedReturnValue}'");
                    var cachedResult = JsonConvert.DeserializeObject(cachedReturnValue, invocation.Method.ReturnType);
                    invocation.ReturnValue = cachedResult;
                }
            }
            else
            {
                invocation.Proceed();
            }
        }

        private bool IsCacheEnabledForThisCall(IInvocation invocation)
        {
            if (invocation.Method.ReturnType == typeof (void))
            {
                Logger.Current.Log("We won't cache calls to void methods");
                return false;
            }

            if (_cachingBehavior == CachingBehavior.CacheAllMethods)
            {
                return true;
            }

            if (_cachingBehavior == CachingBehavior.DontCache)
            {
                return false;
            }

            var cacheEnabled = invocation.Method.CustomAttributes.Any(_ => _.AttributeType == typeof(CacheResultAttribute));
            return cacheEnabled;
        }

        private static void CacheManagerOnUpdate(object sender, CacheUpdateEventArgs<string> cacheUpdateEventArgs)
        {
            Logger.Current.Log($"OnUpdate: {cacheUpdateEventArgs.Region}.{cacheUpdateEventArgs.Key}, Result={cacheUpdateEventArgs.Result.Value}");
        }

        private static void CacheManagerOnRemove(object sender, CacheActionEventArgs cacheActionEventArgs)
        {
            Logger.Current.Log($"OnRemove: {cacheActionEventArgs.Region}.{cacheActionEventArgs.Key}");
        }

        private static void CacheManagerOnPut(object sender, CacheActionEventArgs cacheActionEventArgs)
        {
            Logger.Current.Log($"OnPut: {cacheActionEventArgs.Region}.{cacheActionEventArgs.Key}");
        }

        private static void CacheManagerOnGet(object sender, CacheActionEventArgs cacheActionEventArgs)
        {
            Logger.Current.Log($"OnGet: {cacheActionEventArgs.Region}.{cacheActionEventArgs.Key}");
        }

        private static void CacheManagerOnAdd(object sender, CacheActionEventArgs cacheActionEventArgs)
        {
            Logger.Current.Log($"OnAdd: {cacheActionEventArgs.Region}.{cacheActionEventArgs.Key}");
        }
    }
}