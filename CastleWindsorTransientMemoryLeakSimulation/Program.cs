﻿using System;
using System.Diagnostics;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace CastleWindsorTransientMemoryLeakSimulation
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var container = new WindsorContainer())
            {
                container.Register(Component.For<BigAssClass>().LifestyleTransient());
                //container.Register(Component.For<BigAssClass>().LifestyleSingleton());

                for (var i = 0; i < 999999; i++)
                {
                    var theClass = container.Resolve<BigAssClass>();
                    theClass.BuildUpMemory();
                    //container.Release(theClass); // Always manually release transient components you resolved yourself from the container!

                    Console.WriteLine("Memory usage: {0}", Process.GetCurrentProcess().PrivateMemorySize64);
                }

                Console.ReadLine();
            }
        }
    }
}
