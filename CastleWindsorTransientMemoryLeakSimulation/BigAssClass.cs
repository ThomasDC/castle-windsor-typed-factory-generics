﻿using System;
using System.Collections.Generic;

namespace CastleWindsorTransientMemoryLeakSimulation
{
    public class BigAssClass : IDisposable
    {
        private readonly IList<int> _list;
        private readonly Random _random;

        public BigAssClass()
        {
            _list = new List<int>();
            _random = new Random();
        }

        public void BuildUpMemory()
        {
            for (var i = 0; i < 99999; i++)
            {
                _list.Add(_random.Next(int.MaxValue));
            }
        }

        public void Dispose()
        {
            Console.WriteLine("I'm being disposed!");
        }
    }
}
