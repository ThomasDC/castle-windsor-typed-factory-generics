﻿using System.ServiceModel;

namespace TopShelfCastleWindsorPoc
{
    [ServiceContract]
    public interface IHelloService
    {
        [OperationContract(IsOneWay = true)]
        void Handle(string name);
    }
}
