﻿using Topshelf;

namespace TopShelfCastleWindsorPoc
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>                               
            {
                x.Service<Service>();
                x.RunAsNetworkService();

                x.SetDescription("Topshelf Castle Windsor POC");
                x.SetDisplayName("Top Castle WCF");
                x.SetServiceName("Top Castle WCF");
            });
        }
    }
}
