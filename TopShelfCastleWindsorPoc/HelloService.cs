﻿using System;
using System.Threading;

namespace TopShelfCastleWindsorPoc
{
    public class HelloService : IHelloService
    {
        public HelloService()
        {
            Console.WriteLine("HelloService instantiated");
        }

        public void Handle(string name)
        {
            Console.WriteLine($"Handling '{name}'");
            Thread.Sleep(3000);
            Console.WriteLine($"Finished handling '{name}'");
        }
    }
}