﻿using System;
using System.Linq;
using System.ServiceModel;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Topshelf;

namespace TopShelfCastleWindsorPoc
{
    public class Service : ServiceControl
    {
        private IWindsorContainer _container;

        public bool Start(HostControl hostControl)
        {
            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>();
            _container.Register(Component.For<IHelloService>().ImplementedBy<HelloService>().AsWcfService(new DefaultServiceModel().OnCreated(OnCreated)));
            return true;
        }

        private static void OnCreated(ServiceHost serviceHost)
        {
            var serviceBehavior = (ServiceBehaviorAttribute) serviceHost.Description.Behaviors.Single(_ => _ is ServiceBehaviorAttribute);
            serviceBehavior.ConcurrencyMode = ConcurrencyMode.Single;
            serviceBehavior.InstanceContextMode = InstanceContextMode.Single;
        }

        public bool Stop(HostControl hostControl)
        {
            _container?.Dispose();
            return true;
        }
    }
}