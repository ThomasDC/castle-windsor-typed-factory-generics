﻿using CastleWindsorTypedFactoryPoc.Alerts;

namespace CastleWindsorTypedFactoryPoc
{
    /// <summary>
    /// Castle Windsor will provide the implementation!
    /// Check https://github.com/castleproject/Windsor/blob/master/docs/typed-factory-facility-interface-based.md
    /// </summary>
    public interface IAlertHandlerFactory
    {
        IAlertHandler<T> CreateHandlerFor<T>() where T : BaseAlert;

        void ReleaseHandler<T>(IAlertHandler<T> component) where T : BaseAlert;
    }
}