﻿using System;
using CastleWindsorTypedFactoryPoc.Alerts;

namespace CastleWindsorTypedFactoryPoc
{
    public interface IAlertHandler<in T> : IDisposable where T : BaseAlert
    {
        string Handle(T arg);
    }
}