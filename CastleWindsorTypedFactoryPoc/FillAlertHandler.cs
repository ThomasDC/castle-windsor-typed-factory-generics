﻿using System;
using CastleWindsorTypedFactoryPoc.Alerts;

namespace CastleWindsorTypedFactoryPoc
{
    public class FillAlertHandler : IAlertHandler<FillAlert>
    {
        public string Handle(FillAlert arg)
        {
            return "I'm a FillAlertHandler and I'm handling a FillAlert";
        }

        public void Dispose()
        {
            Console.WriteLine("I'm a FillAlertHandler and I'm being disposed...");
        }
    }
}