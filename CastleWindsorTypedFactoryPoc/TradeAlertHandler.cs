﻿using System;
using CastleWindsorTypedFactoryPoc.Alerts;

namespace CastleWindsorTypedFactoryPoc
{
    public class TradeAlertHandler : IAlertHandler<TradeAlert>
    {
        public string Handle(TradeAlert arg)
        {
            return "I'm a TradeAlertHandler and I'm handling a TradeAlert";
        }

        public void Dispose()
        {
            Console.WriteLine("I'm a TradeAlertHandler and I'm being disposed...");
        }
    }
}