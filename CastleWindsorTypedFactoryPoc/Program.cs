﻿using System;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CastleWindsorTypedFactoryPoc.Alerts;

namespace CastleWindsorTypedFactoryPoc
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var container = new WindsorContainer())
            {
                container.AddFacility<TypedFactoryFacility>();

                container.Register(
                    Component.For<IAlertHandlerFactory>().AsFactory(),
                    Component.For<IAlertHandler<FillAlert>>().ImplementedBy<FillAlertHandler>().LifestyleTransient(),
                    Component.For<IAlertHandler<TradeAlert>>().ImplementedBy<TradeAlertHandler>().LifestyleSingleton());

                var factory = container.Resolve<IAlertHandlerFactory>();

                var handler1 = factory.CreateHandlerFor<FillAlert>();
                var handler2 = factory.CreateHandlerFor<TradeAlert>();
                Console.WriteLine(handler1.Handle(new FillAlert()));
                Console.WriteLine(handler2.Handle(new TradeAlert()));
                factory.ReleaseHandler(handler1); // Will be disposed since lifestyle is transient ('create a new instance everytime')
                factory.ReleaseHandler(handler2); // Won't be disposed since lifestyle is singleton
                
                var handler3 = factory.CreateHandlerFor<FillAlert>();
                var handler4 = factory.CreateHandlerFor<TradeAlert>();
                Console.WriteLine(handler3.Handle(new FillAlert()));
                Console.WriteLine(handler4.Handle(new TradeAlert()));
                factory.ReleaseHandler(handler3);
                factory.ReleaseHandler(handler4);

                Console.WriteLine("FillAlertHandlers are equal: " + (handler1 == handler3)); // are not equals because FillAlertHandler is transient
                Console.WriteLine("TradeAlertHandlers are equal: " + (handler2 == handler4)); // are equal because TradeAlertHandler is singleton

                Console.WriteLine("Shutting down container");
            } // Singletons will be disposed

            Console.WriteLine("All done!");
            Console.ReadLine();
        }
    }
}
